<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'title',
        'status',
    ];

    public function messages()
    {
        return $this->hasMany(TemplatesMessages::class, 'idTemplate', 'id');
    }
}
