<?php

namespace App\Http\Controllers;

use App\Models\Memebresia;
use App\Models\Paquetes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AuthApiController extends Controller
{
    public function signUp(Request $request)
    {
        $user = User::create([
            'uuid' => Str::uuid(),
            'name' => $request->name,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $paquete_free = Paquetes::where('proof', 1)->first();

        if($paquete_free != null)
        {
            $date = Carbon::parse(date('Y-m-d'));
            $endDate = $date->addDay($paquete_free->daysDuration);
            $membresia = Memebresia::create([
                'uuid' => Str::uuid(),
                'idPackage' => $paquete_free->id,
                'idUser' => $user->id,
                'start' => date('Y-m-d'),
                'end' => $endDate,
                'idPay' => 0
            ]);

            $user->idMembresia = $membresia->id;
            $user->save();
        }

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(3);
        $token->save();

        return response()->json([
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
