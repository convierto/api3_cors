<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([ 'prefix' => 'auth' ], function () {
    Route::post('login', [AuthApiController::class, 'login']);
    Route::post('signup', [AuthApiController::class, 'signUp']);

    Route::group([ 'middleware' => 'auth:api' ], function() {
        Route::post('logout', [AuthApiController::class, 'logout']);
        Route::post('user', [AuthApiController::class, 'user']);
    });
});


ROute::get('users', [UserController::class, 'index']);
